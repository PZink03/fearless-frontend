function createCard(name, description, pictureUrl, date, dateEnd, locations ) {
    return `
    <div class="col-4">
        <div class="conference-card">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-text">${description}</p>
            <div class="card-footer">
            <small class="text-muted">${date} - ${dateEnd}</small>
            </div>
            <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h6 class="card-subtitle mb-2 text-muted">${locations}</h6>

            </div>
            </div>
        </div>
        </div>
        </div>
        </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const dateStart =  new Date(details.conference.starts)
            const date = dateStart.toLocaleDateString()
            const dateE = new Date(details.conference.ends)
            const locations = details.conference.location.name
            const dateEnd = dateE.toLocaleDateString()
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, date, dateEnd, locations);
            const column = document.querySelector('.row');
            column.innerHTML += html;;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });








// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';


//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;


//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();

//           const description = details.conference.description
//           const detailTag = document.querySelector('.card-text')
//           detailTag.innerHTML = description

//           const imageTag = document.querySelector('.card-img-top')
//           imageTag.src = details.conference.location.picture_url;

//         }
//         console.log(details)

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           console.log(details);
//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });
