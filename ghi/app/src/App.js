import Nav from './Nav';
import React from 'react';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import { BrowserRouter } from "react-router-dom"
import { Route } from 'react-router-dom';
import { Routes } from 'react-router-dom';
import AttendConferenceForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />}></Route>
            <Route path = "locations">
              <Route path="new" element={<LocationForm/>} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm/>} />
            </Route>
            <Route path="attendees">
              <Route path="new" element={<AttendConferenceForm/>} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />}/>
            </Route>

            {/* <ConferenceForm/> */}
            {/* <LocationForm/> */}
            {/* <AttendeesList attendees={props.attendees} /> */}

          </Routes>
    </BrowserRouter>
  );
}

export default App
