import React, {useEffect, useState} from 'react';

function ConferenceForm (props) {
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {

    const url = 'http://localhost:8000/api/locations/'

    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        setLocations(data.locations)

    }
}

const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.description = description;
    data.starts = starts;
    data.ends = ends;
    data.max_presentations = max_presentations
    data.max_attendees = max_attendees
    data.location = location
    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentation('');
      setMaxAttendees('');
      setLocation('')
    }
  }
const [name, setName] = useState('');
const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }
  const [starts, setStarts] = useState('');
  const handleDateStartChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }
  const [ends, setEnds] = useState('');
  const handleDateEndChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }
  const [description, setDescription] = useState('');
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }
  const [max_presentations, setMaxPresentation] = useState('');
  const handleMaximumPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentation(value);
  }
  const [max_attendees, setMaxAttendees] = useState('');
  const handleMaximumAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }
  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

useEffect(() => {
    fetchData();
  }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateStartChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateEndChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" name="description" id="description" className="form-control"></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaximumPresentationsChange} value={max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaximumAttendeesChange} value={max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}

                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
